const relations = [
        {
            value: "16x9",
            label: "16 x 9 - Widescreen",
            resolutions: [
                "1920 x 1080",
                "1366 x 768"
            ]
        },
        {
            value: "16x10",
            label: "16 x 9 - Widescreen",
            resolutions: [
                "1680 x 1050"
            ]
        },
        {
            value: "otro",
            label: "16 x 9 - Widescreen",
            resolutions: [
                "1680 x 1050"
            ]
        },
        {
            value: "otro más",
            label: "16 x 9 - Widescreen",
            resolutions: [
                "1680 x 1050"
            ]
        },
        {
            value: "uno diferente",
            label: "16 x 9 - Widescreen",
            resolutions: [
                "1680 x 1050"
            ]
        },
    ]

    
function addOptions() {
    var contenido;
    for (i = 0; i < relations.length; i++) {
        var option = document.createElement("option");
        contenido = relations[i].value
        option.appendChild(document.createTextNode(contenido));
        console.log(relations[i].value)
        
        document.querySelector("#aspectRatioValues").appendChild(option);
    }
}
addOptions();
